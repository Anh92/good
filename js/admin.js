angular.module('myApp', ['ngMaterial']).config(function ($mdThemingProvider) {
    $mdThemingProvider.theme('red')
        .primaryPalette('red');

    $mdThemingProvider.theme('blue')
        .primaryPalette('blue');

})
    .controller('MovieController', function ($scope, $http, $mdDialog, $interval) {
        $scope.$watch('search', function () {
            $scope.getTruyenNganDub();
            $scope.getTruyenDaiDub();
            $scope.getTruyenNganSub();
            $scope.getTruyenDaiSub();
        });

        $scope.title = "Chúc các bạn xem phim vui vẻ";
        $scope.isTruyenNgan = 1;
        $scope.selectedId = 1 + 'dub';
        $scope.truyendaidub = null;
        $scope.truyenngandub = null;
        $scope.truyendaisub = null;
        $scope.truyenngansub = null;
        $scope.getTruyenNganDub = function () {
            $http({
                method: 'GET',
                url: "../api/truyenngandub/"
            }).
                then(function (response) {
                    $scope.truyenngandub = response.data;
                }).catch(function onError(response) {

                });

        }
        $scope.getTruyenDaiDub = function () {
            $http({
                method: 'GET',
                url: "../api/truyendaidub/"
            }).
                then(function (response) {
                    $scope.truyendaidub = response.data;
                }).catch(function onError(response) {

                });
        }
        $scope.getTruyenNganSub = function () {
            $http({
                method: 'GET',
                url: "../api/truyenngansub/"
            }).
                then(function (response) {
                    $scope.truyenngansub = response.data;
                }).catch(function onError(response) {

                });

        }
        $scope.getTruyenDaiSub = function () {
            $http({
                method: 'GET',
                url: "../api/truyendaisub/"
            }).
                then(function (response) {
                    $scope.truyendaisub = response.data;
                }).catch(function onError(response) {

                });
        }

        $scope.select = function (table, item) {
            $scope.showAdvanced(table, item)
        }

        $scope.addnew = function (table) {
            $scope.showAdvanced(table, {})
        }

        $scope.theme = 'red';

        var isThemeRed = true;

        $interval(function () {
            $scope.theme = isThemeRed ? 'blue' : 'red';

            isThemeRed = !isThemeRed;
        }, 2000);

        $scope.showAdvanced = function (table, item) {
            $mdDialog.show({
                controller: DialogController,
                templateUrl: 'dialog1.tmpl.html',
                parent: angular.element(document.body),
                clickOutsideToClose: true,
                locals: {
                    table,
                    item,
                    token: $scope.token
                }
            })
                .then(function (answer) {
                    if (answer == 'answer') {
                        $scope.getTruyenNganDub();
                        $scope.getTruyenDaiDub();
                        $scope.getTruyenNganSub();
                        $scope.getTruyenDaiSub();
                    }
                }, function () {
                    $scope.status = 'You cancelled the dialog.';
                });
        };

        function DialogController($scope, $mdDialog, locals) {
            $scope.item = locals.item;
            $scope.item.table = locals.table;
            $scope.item.token = locals.token;
            $scope.choices = [];
            if ($scope.item.source != null) {
                $scope.choices = JSON.parse($scope.item.source);
            }
            $scope.hide = function () {
                $mdDialog.hide();
            };

            $scope.cancel = function () {
                $mdDialog.cancel();
            };
            $scope.add = function () {
                $mdDialog.hide("answer");
                $scope.item.source = angular.toJson($scope.choices);
                $http({
                    method: 'POST',
                    url: "../api/add/",
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                    transformRequest: function (obj) {
                        var str = [];
                        for (var p in obj)
                            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                        return str.join("&");
                    },
                    data: $scope.item
                }).
                    then(function (response) {
                        $mdDialog.hide("answer");
                    }).catch(function onError(response) {
                        $mdDialog.hide("error");
                    });
            };

            $scope.delete = function () {
                $mdDialog.hide("answer");
                $http({
                    method: 'POST',
                    url: "../api/delete/",
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                    transformRequest: function (obj) {
                        var str = [];
                        for (var p in obj)
                            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                        return str.join("&");
                    },
                    data: $scope.item
                }).
                    then(function (response) {
                        $mdDialog.hide("answer");
                    }).catch(function onError(response) {
                        $mdDialog.hide("error");
                    });
            };

            $scope.edit = function () {
                $mdDialog.hide("answer");
                $scope.item.source = angular.toJson($scope.choices);
                $http({
                    method: 'POST',
                    url: "../api/edit/",
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                    transformRequest: function (obj) {
                        var str = [];
                        for (var p in obj)
                            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                        return str.join("&");
                    },
                    data: $scope.item
                }).
                    then(function (response) {
                        $mdDialog.hide("answer");
                    }).catch(function onError(response) {
                        $mdDialog.hide("error");
                    });
            };

            // $scope.choices = [{ type: 'file',url:'http://url1' }, { type: 'frame',url:'http://url2' }, { type: 'file', url:'http://url3' }];
            $scope.addNewChoice = function () {
                $scope.choices.push({ type: '', url: '' });
            };
            $scope.deleteChoice = function (index) {
                $scope.choices.splice(index, 1);
            };
        }
    });