angular.module('myApp', [])
    .controller('MovieController', function ($scope, $sce, $http) {
        // $scope.$watch('search', function () {
        //     $scope.URLToArray(window.location.search);
        // });

        $scope.title = "Doremon Việt chúc các bạn xem phim vui vẻ";
        $scope.startEp = 0;
        $scope.typeTruyen = 'truyen-ngan';
        $scope.selectedId = undefined;
        $scope.mainData = allDataGet;
        $scope.isVideo = false;
        $scope.movieData = currentEp;
        $scope.mainEp = null;
        $scope.movieSource = { url: "" };
        $scope.movieSources = [];
        $scope.listEp = [];
        $scope.server = 0;
        $scope.movieUrl = "";
        $scope.repareData = function () {
            var rangEp = 100;
            if ($scope.typeTruyen == 'truyen-dai') {
                rangEp = 15;
            }
            var maxEp = Math.ceil($scope.mainData.length / rangEp);
            for (var i = 0; i < maxEp; i++) {
                var lastEp = ((i + 1) * rangEp > $scope.mainData.length) ? $scope.mainData.length : (i + 1) * rangEp;
                $scope.listEp.push({
                    title: "Tập " + (i * rangEp + 1) + " - " + lastEp,
                    start: i * rangEp,
                    end: lastEp
                });

            }
            if ($scope.listEp.length > 0) {
                epIndex = Math.floor(epIndex / rangEp);
                $scope.renMainEp($scope.listEp[epIndex].start, $scope.listEp[epIndex].end);
            }
            $scope.title = $scope.movieData.title;
            if ($scope.server > $scope.movieData.source.length - 1) {
                $scope.server = 0;
            }
            $scope.movieSources = JSON.parse($scope.movieData.source);
            $scope.movieSource = $scope.movieSources[$scope.server];
            if ($scope.movieSource.type == 'file') {
                $scope.isVideo = true;
            }
            $scope.movieUrl = $sce.trustAsResourceUrl($scope.movieSource.url);
        }
        $scope.renMainEp = function (start, end) {
            $scope.startEp = start;
            $scope.mainEp = $scope.mainData.slice(start, end);
        }

        $scope.selectServer = function (sever) {
            var curLocation = window.location;
            window.location.href = curLocation.origin + curLocation.pathname + "?xem-doraemon=" + $scope.typeTruyen + "&xem-doraemon-tap=" + $.$scope.mainData.ep + "&xem-doraemon-server=" + sever;
        }

        $scope.select = function (id) {
            var curLocation = window.location;
            window.location.href = curLocation.origin + curLocation.pathname + "?xem-doraemon=" + $scope.typeTruyen + "&xem-doraemon-tap=" + id;
        }

        $scope.endsWith = function (str, suffix) {
            return str.indexOf(suffix, str.length - suffix.length) !== -1;
        }
        $scope.chooseType = function (typeTruyen) {
            var curLocation = window.location;
            window.location.href = curLocation.origin + curLocation.pathname + "?xem-doraemon=" + typeTruyen;
        }
        $scope.repareData();
    });