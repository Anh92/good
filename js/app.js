angular.module('myApp', [])
    .controller('MovieController', function ($scope, $sce, $http) {
        $scope.$watch('search', function () {
            $scope.URLToArray(window.location.search);
        });

        $scope.title = "Doremon Việt chúc các bạn xem phim vui vẻ";
        $scope.startEp = 0;
        $scope.typeTruyen = 1;
        $scope.selectedId = undefined;
        $scope.movieId = undefined;
        $scope.mainData = null;
        $scope.isVideo = false;
        $scope.movieData = null;
        $scope.mainEp = null;
        $scope.movieSource = { url: "" };
        $scope.movieSources = [];
        $scope.listEp = [];
        $scope.server = 0;
        $scope.movieUrl = "";
        $scope.getData = function (url) {
            $http({
                method: 'GET',
                url: url
            }).
                then(function (response) {
                    $scope.mainData = response.data;
                    var rangEp = 100;
                    if ($scope.typeTruyen) {
                        rangEp = 15;
                    }
                    var maxEp = Math.ceil($scope.mainData.length / rangEp);
                    for (var i = 0; i < maxEp; i++) {
                        var lastEp = ((i + 1) * rangEp > $scope.mainData.length) ? $scope.mainData.length : (i + 1) * rangEp;
                        $scope.listEp.push({
                            title: "Tập " + (i * rangEp + 1) + " - " + lastEp,
                            start: i * rangEp,
                            end: lastEp
                        });

                    }

                    var epIndex = 0;
                    if ($scope.movieId != undefined) {
                        epIndex = $scope.mainData.findIndex(function (element) { return element.id == $scope.movieId; });
                        $scope.movieData = $scope.mainData.find(function (element) { return element.id == $scope.movieId; });
                    } else {
                        $scope.movieData = $scope.mainData[0];
                        $scope.movieId = $scope.movieData.id;
                    }
                    if ($scope.listEp.length > 0) {
                        epIndex = Math.floor(epIndex / rangEp);
                        $scope.renMainEp($scope.listEp[epIndex].start, $scope.listEp[epIndex].end);
                    }
                    $scope.title = $scope.movieData.title;
                    if ($scope.server > $scope.movieData.source.length - 1) {
                        $scope.server = 0;
                    }
                    $scope.movieSources = JSON.parse($scope.movieData.source);
                    $scope.movieSource = $scope.movieSources[$scope.server];
                    if ($scope.movieSource.type == 'file') {
                        $scope.isVideo = true;
                    }
                    $scope.movieUrl = $sce.trustAsResourceUrl($scope.movieSource.url);
                }).catch(function onError(response) {

                });

        }
        $scope.renMainEp = function (start, end) {
            $scope.startEp = start;
            $scope.mainEp = $scope.mainData.slice(start, end);
        }

        $scope.selectServer = function (sever) {
            var curLocation = window.location;
            window.location.href = curLocation.origin + curLocation.pathname + "?xem-doraemon-truyen-ngan=" + $scope.typeTruyen + "&xem-doraemon-tap=" + $scope.movieId + "&xem-doraemon-server=" + sever;
        }

        $scope.select = function (id) {
            var curLocation = window.location;
            window.location.href = curLocation.origin + curLocation.pathname + "?xem-doraemon-truyen-ngan=" + $scope.typeTruyen + "&xem-doraemon-tap=" + id;
        }

        $scope.URLToArray = function (url) {
            var request = {};
            var arr = [];
            var pairs = [];
            if (url.indexOf('&') == -1) {
                pairs = url.substring(url.indexOf('?') + 1).split('/');
            } else {
                pairs = url.substring(url.indexOf('?') + 1).split('&')
            }

            for (var i = 0; i < pairs.length; i++) {
                var pair = pairs[i].split('=');

                //check we have an array here - add array numeric indexes so the key elem[] is not identical.
                if ($scope.endsWith(decodeURIComponent(pair[0]), '[]')) {
                    var arrName = decodeURIComponent(pair[0]).substring(0, decodeURIComponent(pair[0]).length - 2);
                    if (!(arrName in arr)) {
                        arr.push(arrName);
                        arr[arrName] = [];
                    }

                    arr[arrName].push(decodeURIComponent(pair[1]));
                    request[arrName] = arr[arrName];
                } else {
                    request[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1]);
                }
            }
            if (request != undefined) {
                if (request['xem-doraemon-truyen-ngan'] == '1') {
                    $scope.typeTruyen = 1;
                } else {
                    $scope.typeTruyen = 0;
                }
                if (request['xem-doraemon-server']) {
                    $scope.server = request['xem-doraemon-server'];
                }
                $scope.movieId = request['xem-doraemon-tap'];
                if ($scope.typeTruyen == 0) {
                    $scope.getData("./api/get-series");
                } else {
                    $scope.getData("./api/get-extra");
                }
            } else {
                $scope.getData("./api/get-series");
            }
        }
        $scope.endsWith = function (str, suffix) {
            return str.indexOf(suffix, str.length - suffix.length) !== -1;
        }
        $scope.chooseType = function (typeTruyen) {
            var curLocation = window.location;
            window.location.href = curLocation.origin + curLocation.pathname + "?xem-doraemon-truyen-ngan=" + typeTruyen;
        }
    });