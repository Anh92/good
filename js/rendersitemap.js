angular.module('myApp', [])
    .controller('MovieController', function ($scope, $sce, $http) {
        $scope.head = '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:video="http://www.google.com/schemas/sitemap-video/1.1">';
        $scope.end = '</urlset>';
        $scope.renData = function (item) {
            var movieSource = JSON.parse(item.source);
            movieSource = movieSource[0].url.split('/');
            var datare = '<url><loc>http://doraemonviet.tk/?xem-doraemon-truyen-ngan=0&xem-doraemon-thuyet-minh=0&xem-doraemon-tap=' + item.id + '</loc><video:video>';
            datare += '<video:thumbnail_loc>https://img.youtube.com/vi/' + movieSource[4] + '/0.jpg</video:thumbnail_loc><video:title>';
            datare += '<![CDATA[ ' + item.title + ' ]]></video:title><video:description><![CDATA[ ' + item.title + ' ]]></video:description><video:view_count>172833</video:view_count>';
            datare += '<video:family_friendly>yes</video:family_friendly><video:live>no</video:live><video:publication_date>2017-12-16T10:20:39+07:00</video:publication_date>';
            datare += '<video:player_loc allow_embed="yes" autoplay="ap=1">https://www.youtube.com/v/' + movieSource[4] + '</video:player_loc>';
            datare += '<video:uploader info="doraemonviet.tk">doraemonviet.tk</video:uploader></video:video></url>';
            return datare;

        }
        $scope.getData = function () {
            $http({
                method: 'GET',
                url: 'http://doraemonviet.tk/api/truyendaidub/'
            }).
                then(function (response) {
                    $scope.mainData = response.data;

                }).catch(function onError(response) {

                });

        }
        $scope.getData();
    });