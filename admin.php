
<?php
if(!isset($_GET['admin']) || $_GET['admin'] !='anhphihoang')
{
  echo '<html><head>
  <title>404 Not Found</title>
  <style></style></head><body cz-shortcut-listen="true">
  <h1>Not Found</h1>
  <p>The requested URL was not found on this server.</p>
  <p>Additionally, a 404 Not Found
  error was encountered while trying to use an ErrorDocument to handle the request.</p>
  
  </body></html>';
  return;
}
?>
<!DOCTYPE html>
<html lang="en-US" ng-app="myApp" ng-controller="MovieController">

<head>
    <link rel="stylesheet" type="text/css" media="all" href="./css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" media="all" href="./css/responsive.css" />
    <link rel='stylesheet' id='theme53261-css' href='./css/main-style.css' type='text/css' media='all' />
    <link href="./css/video-js.css" rel="stylesheet">
    <link rel="stylesheet" href="./css/angular-material.min.css">
</head>

<body class="single single-post postid-1984 single-format-video cat-13-id cherry-fixed-layout">
    <div id="motopress-main" class="main-holder" style="z-index: auto;">

        <div class="motopress-wrapper content-holder clearfix">
            <div class="post_content">
                <input type="text" ng-model="token"/>
                <h1>Thuyết minh dài</h1>
                <button class="button1" ng-click="addnew('daidub')" >Thêm tập</button>                
                <div id="longtiengdai" class="tabcontent">
                    <button ng-repeat="item in truyendaidub" class="button1" ng-click="select('daidub',item)" >{{item.ep}}</button>
                </div>
                <h1>Phụ đề dài</h1>
                <button class="button1" ng-click="addnew('daisub')" >Thêm tập</button>  
                <div id="phudedai" class="tabcontent">
                    <button ng-repeat="item in truyendaisub" class="button1" ng-click="select('daisub',item)">{{item.ep}}</button>
                </div>
                <h1>Thuyết minh ngắn</h1>
                <button class="button1" ng-click="addnew('ngandub')" >Thêm tập</button>  
                <div id="phude" class="tabcontent">
                    <button ng-repeat="item in truyenngandub" class="button1" ng-click="select('ngandub',item)">{{item.ep}}</button>
                </div>
                <h1>Phụ đề ngắn</h1>
                <button class="button1" ng-click="addnew('ngansub')" >Thêm tập</button>  
                <div id="phude" class="tabcontent">
                    <button ng-repeat="item in truyenngansub" class="button1" ng-click="select('ngansub',item)">{{item.ep}}</button>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <footer class="motopress-wrapper footer">
            <div class="container">
            </div>
        </footer>
        <!--End #motopress-main-->
    </div>
     <script src="./js/angular.min.1.js"></script>
    <script src="./js/angular-animate.min.js"></script>
    <script src="./js/angular-aria.min.js"></script>
    <script src="./js/angular-messages.min.js"></script>
    <!-- Angular Material Library -->
    <script src="./js/angular-material.min.js"></script>
    <script src="./js/admin.js"></script>
</body>

</html>